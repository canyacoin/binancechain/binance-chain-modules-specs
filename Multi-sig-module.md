
# MULTISIG MODULE

JUNE 2019

TEAM CANYA


## PROJECT OVERVIEW

This document covers the multi-signature module that will be built as a Cosmos module for Binance Chain. This will serve as the basis for the escrow, hedged escrow and DAO modules later. 

NON-INTERACTIVE MULTI-SIGNATURE MODULE
This module using Cosmos multi-key store to persist state about the multi-signature wallets on-chain and is non-interactive since signers don’t need to be online at the same time. Any level of m of n can be set, and the wallet can be safely reused to save data. 

TesTING
This module will be deployed and tested on a v0.33.0 Cosmos base-app with a local chain (single node) running. It will also be tested in the Byzantine environment with 4 nodes running the network. 

# SPEC

Cosmos Stack
Set: [interface <-> msgs <-> handler <-> keeper <-> state]
Get: [interface <-> querier <-> keeper <-> state]

Types:
```
musig Musig struct{
Name: string 		// unique name of wallet 
WalletAddr: string	// bech32 address of Wallet
publicKey: string	// publicKey to sign from wallet
mValue: string 	// threshold
nValue: string	// number of signers
[]Signers: signer	// array of signers
txData: tx		// txData to sign (struct)
[]signedTx: string	// array of signed tx
}
```

Keeper:
```
getMusig(ID) && setMusig(ID)
getSigners(ID) && getMValue(ID) && getNValue(ID)
getTxData(ID) && getSignedTx(ID)
getMusigID(signer) && getMusigID(addr)
getAddress(ID) && getPubKey(ID)

setSigner(ID) && setAddress(ID)
setSignedTX(ID, signer) 
incrNonce()
```

Wallet Generation
The multi-signature wallet is generated by one of the intended parties calling the handler for the module which sets a new multi-signature wallet to their address public key. The wallet is given an identifier so that a lookup registrar can be used. Parameters specify the signature threshold. The state is stored in multi-key storage.

1.1 genMuSig(m, n)	//add multi-sig parameters
	-> returns new walletID 
```
Set
Msgs:
genMuSig()

Handler:
genMuSig()

Keeper:
setMuSig()
```

The other participants call the register function which registers their public addresses and keys to the wallet. This state is stored in multi-key storage.

1.2 setSigner(ID)	//register owner key in wallet
	-> registers signer public key
```
Set
Msgs: 
setSigner(ID)

Handler:
setSigner(ID)

Keeper:
setSigner(ID, signerPublicKey)
```


At this point, all three public keys of parties have been registered in the wallet. Any member can now call wallet generation handler which computes a compliant BECH32 BNB address from the parties’ public keys and is stored in the multi-key storage. 

Any funds sent to this address will require signatures from parties with the correct threshold specified in the wallet generation.

1.3 genAddr(walletID)	//generate the multi-sig wallet
	-> returns walletAddress
```
Set
Msgs: 
generateAddress(ID)

Handler:
generateAddress(ID)

Keeper:
setAddress(ID)
-> check for 3 publicKeys, or return fail
-> combine 3 keys 
-> compute bech32 from keys, address from key          
-> set key and address
```

Compute burden:
The state machine needs to compute all combinations of nCr (3 Choose 2 = 3), but large multi-sigs will lead to runaway compute cost. 

Alternative is to get the public keys, compute all combinations of nCr locally, and insert the signing keys back into the wallet state instead. This takes the burden off the state machine to compute the combinations. (May not be a big deal - pay more fees?).Signing Generation
When funds need to be sent from the wallet, one member generates the transaction data, and registers it to the wallet, calling the following function:

1.4 registerTx(accTo, bal, nonce)	//register the desired tx
	-> registers unsigned transaction data

```
Set
Msgs: 
registerTx(ID, accTo, bal, Asset, Memo, Data, SourceID, Nonce)

Handler:
registerKey(ID, accTo, bal, Asset, Memo, Data, SourceID, Nonce)

Keeper:
setTxData(ID, accTo, bal, Asset, Memo, Data, SourceID, Nonce)
```

Each party can now query the intended transaction in order to generate a signed transaction.

1.5 queryTx(walletID)
	-> returns intended unsigned transaction data
```
Get
Querier: 
queryTx(ID) || queryTx(ID, Nonce)

Keeper:
getTx(ID) || getTx(ID, Nonce)
```

This is then registered in the wallet by calling the submit function.

1.6 submitSignedTx(walletID, nonce, signedData)
	-> registers partially-signed transaction data from signer
```
Set
Msgs: 
submitTx(ID, data, Nonce)

Handler:
submitTx(ID, data, Nonce)

Keeper:
setSignedTx(ID, data, Nonce)
-> if first, then set
-> if after, then set and return signedTx[0:]
```

Once the correct threshold of secrets have been registered in the wallet, anyone can then broadcast that to a node and update the chain state. The wallet can be used again by incrementing the nonce, which invalidates all previous signatures..

1.7 spendTx(walletID, nonce)
	-> broadcasts transaction and updates wallet
```
Set
Msgs: 
spendTx(ID, data, Nonce)

Handler:
spendTx(ID, data, Nonce)
-> broadcast the first tx via normal spend
-> incrNonce(ID)

Keeper:
incrNonce(ID)
-> increment the nonce and set
```


Other Aspects
Fees. The creation of a multi-sig should attract a 0.01-0.1 BNB fee per signer, since it adds cost to store details on the blockchain. Spending should also attract 0.02-0.2 BNB fee since it further adds state to the blockchain. 

Data Use. A 2 of 3 multi-signature will require storing three 64byte public keys on chain, with up to 32 bytes of extra data, with 32 bytes for the final wallet address — for a total of <300bytes. Spending from a multi-signature will require as a minimum one unsigned transaction (128 bytes), two partially-signed transactions (252 bytes) and one fully-signed transaction (128 bytes) — total of 512 bytes. 

Off-chain Option. Parties can elect to do signing of the transaction purely offline (to save fees). In this case if a nonce was not allocated by a registerTx call, then nothing further needs to be done. If a nonce was already allocated then it can be reused. If a nonce was already signed, then it cannot be reused.